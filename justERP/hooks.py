# -*- coding: utf-8 -*-
from __future__ import unicode_literals

app_name = "justERP"
app_title = "justERP"
app_publisher = "justERP"
app_description = "settings for justERP"
app_icon = "icon icon-signal"
app_color = "grey"
app_email = "info@justERP.de"
app_version = "0.0.1"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/justERP/css/justERP.css"
# app_include_js = "/assets/justERP/js/justERP.js"

# include js, css files in header of web template
# web_include_css = "/assets/justERP/css/justERP.css"
# web_include_js = "/assets/justERP/js/justERP.js"

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "justERP.install.before_install"
# after_install = "justERP.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "justERP.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"justERP.tasks.all"
# 	],
# 	"daily": [
# 		"justERP.tasks.daily"
# 	],
# 	"hourly": [
# 		"justERP.tasks.hourly"
# 	],
# 	"weekly": [
# 		"justERP.tasks.weekly"
# 	]
# 	"monthly": [
# 		"justERP.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "justERP.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "justERP.event.get_events"
# }

